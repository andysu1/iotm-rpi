#! /usr/bin env python3

import paho.mqtt.client as mqtt
import ssl
import socket
from dotenv import load_dotenv
from os import getenv

load_dotenv()

# ======================================CONSTANTS
# relay - read doc from https://vkmodule.com.ua/Ethernet/pdf/Protocol%20Ethernet-modules%20Ru.pdf
TOPIC = getenv('topic')
RELAY_HOSTS = [
    {
        "id": "rel1",
        "type": "socket-4",
        "addr": getenv('rel1Addr'),
        "port": int(getenv('rel1Port')),
        "addrGet": 23
    },
    {
        "id": "rel2",
        "type": "socket-4",
        "addr": getenv('rel2Addr'),
        "port": int(getenv('rel2Port')),
        "addrGet": 44
    },
]

RELAY_SENSORS = [
    {
        "name": "Електрокотел",
        "host": "rel1",
        "id": "sw1",
        "type": "switch",
        "num": 1,
        "setOn": "\x22\x00\x01\x00",
        "setOff": "\x22\x00\x00\x00"
    },
    {
        "name": "Автоматика ГАЗ",
        "host": "rel1",
        "id": "sw2",
        "type": "switch",
        "num": 2,
        "setOn": "\x22\x01\x01\x00",
        "setOff": "\x22\x01\x00\x00"},
    {
        "name": "Насос БОЙЛЕР",
        "host": "rel1",
        "id": "sw3",
        "type": "switch",
        "num": 3,
        "setOn": "\x22\x02\x01\x00",
        "setOff": "\x22\x02\x00\x00"
    },
    {
        "name": "Газ КОТЕЛ",
        "host": "rel1",
        "id": "sw4",
        "type": "switch",
        "num": 4,
        "setOn": "\x22\x03\x01\x00",
        "setOff": "\x22\x03\x00\x00"},
    {
        "name": "Вода басейн",
        "host": "rel1",
        "id": "sw5",
        "type": "switch",
        "num": 5,
        "setOn": "\x22\x04\x01\x00",
        "setOff": "\x22\x04\x00\x00"},
    {
        "name": "Опалення ДІМ",
        "host": "rel1",
        "id": "sw6",
        "type": "switch",
        "num": 6,
        "setOn": "\x22\x05\x01\x00",
        "setOff": "\x22\x05\x00\x00"
    },
    {
        "name": "Опалення БАСЕЙН",
        "host": "rel1",
        "id": "sw7",
        "type": "switch",
        "num": 7,
        "setOn": "\x22\x06\x01\x00",
        "setOff": "\x22\x06\x00\x00"
    },
    {
        "name": "Температура ПОДАЧА",
        "host": "rel2",
        "id": "temp9",
        "type": "thermo",
        "num": 9
    },
    {
        "name": "Температура ОБРАТКА",
        "host": "rel2",
        "id": "temp10",
        "type": "thermo",
        "num": 10
    },
]
# ======================================CONSTANTS END


def read_sensors(host, port, addr):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))
    s.sendall(bytes.fromhex(addr))
    data = s.recv(24)
    s.close()
    return[i for i in data[1:]]


def write_sensor(id, onoff):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    relayId = next(item for item in RELAY_SENSORS if item["id"] == id)['host']
    host = next(item for item in RELAY_HOSTS if item["id"] == relayId)['addr']
    port = int(next(item for item in RELAY_HOSTS if item["id"] == relayId)['port'])
    s.connect((host, port))
    mess = ''
    if onoff == '1':
        mess = next(item for item in RELAY_SENSORS if item["id"] == id)['setOn']
    if onoff == '0':
        mess = next(item for item in RELAY_SENSORS if item["id"] == id)['setOff']
    # print(f'host={host}, port={port}, message={bytes(mess,"utf-8")}')
    s.sendall(bytes(mess, 'utf-8'))
    data = s.recv(24)
    client.publish(TOPIC, 'HELLO')
    s.close()


def config_sw(num, name, onoff):
    status = f'''
    {{    
    "widget": "toggle",    
    "descr": "{name}",    
    "topic": "{TOPIC}/dom/sw{num}",    
    "color": "green",
    "colorOff": "orange", 
    "status": "{onoff}"
    }}
    '''
    return status


def config_temp(num, name='', temp=''):
    status = f'''
    {{    
    "widget": "anydata",    
    "descr": "{name}",    
    "topic": "{TOPIC}/dom/temp{num}",    
    "icon": "thermometer",  
    "color": "orange", 
    "status": "{temp}",
    "after": "°C"    
    }}
    '''
    return status


def send_config():
    data = []
    for i in RELAY_HOSTS:
        data += read_sensors(i['addr'], i['port'], str(i['addrGet']))
    for i, k in enumerate(RELAY_SENSORS):
        if k['type'] == 'switch':
            client.publish(f'{TOPIC}/dom/config', config_sw(k['num'], k['name'], data[i]))
        if k['type'] == 'thermo':
            client.publish(f'{TOPIC}/dom/config', config_temp(k['num'], k['name'], data[i+1]
                                                              if data[i+1] < 128 else 128-data[i+1]))


def on_connect(client, userdata, flags, rc):
    client.subscribe(f'{TOPIC}/#')


def on_message(client, userdata, msg):
    if str(msg.payload.decode('utf-8')) == 'HELLO':
        print('recieve status request!')
        send_config()
    if msg.topic.find('/control') > 0:
        try:
            status = msg.payload.decode("utf-8")
            id = msg.topic.split('/')[-2]
            write_sensor(id, status)
            print(id)
            client.publish(msg.topic.replace('/control', '/status'), f'{{"status": "{status}"}}')
        except Exception as e:
            print(e)


client = mqtt.Client(transport="websockets")
client.tls_set(tls_version=ssl.PROTOCOL_TLSv1_2)
client.username_pw_set(getenv('name'), getenv('passw'))
client.on_connect = on_connect
client.on_message = on_message
client.connect(getenv('host'), int(getenv('port')), 60)
client.loop_forever()
